import { element } from 'protractor';
import { Component, OnInit, Input } from '@angular/core';

export interface Tour {
	number: number,
	firstAttempt: number,
	secondAttempt: number,
	thirdAttempt?: number,
	finalScore: number,
	isStrike: boolean,
	isSpare: boolean,
	isLast?: boolean
}

@Component({
	selector: 'app-user-score',
	templateUrl: './user-score.component.html',
	styleUrls: ['./user-score.component.css']
})
export class UserScoreComponent implements OnInit {
	currentTour: number = 0;
	attempt: number;
	finalResult: number = 0;
	@Input() name;
	
	constructor() { }

	public steps: Array<Tour> = [];

	ngOnInit() {
		this.initStepsArray();
		this.attempt = 1;
		console.log(this.steps, 'step')
	}

	initStepsArray(): void {
		for (let i = 0; i < 10; i++) {
			this.steps.push({
				number: i,
				firstAttempt: 0,
				secondAttempt: 0,
				thirdAttempt: 0,
				finalScore: 0,
				isStrike: false,
				isSpare: false,
			})
		}
	}

	setScore(result: number, attempt: number): void {
		if(this.currentTour === 9) {
			if(attempt === 1) {
				this.firstLastAttempt(result);
			} else if (attempt === 2) {
				this.secondLastAttempt(result);
			} else if (attempt === 3) {
				this.thirdAttempt(result);
			}	
			return ;	
		}

		if(attempt === 1) {
			this.firstAttempt(result);
		} else if (attempt === 2) {
			this.secondAttempt(result);			
			this.nextTour();
		}
	}
	
	
	firstAttempt(result: number): void {
		let currentTour: Tour = this.steps[this.currentTour];
		
		currentTour.firstAttempt = result;
		this.attempt = 2;
		
		//if spare in prev tour
		if(this.currentTour > 0 && this.steps[this.currentTour - 1].isSpare) {
			this.steps[this.currentTour - 1].finalScore = this.steps[this.currentTour - 1].finalScore + result;
		}
		
		//if strike in prev tour
		if(this.currentTour > 0 && this.steps[this.currentTour - 1].isStrike) {
			if(this.currentTour > 1 && this.steps[this.currentTour - 1].isStrike && this.steps[this.currentTour - 2].isStrike) {
				this.steps[this.currentTour - 2].finalScore += result;
			}
			this.steps[this.currentTour - 1].finalScore += result;
		}
		
		//check strike
		if(result === 10) {
			console.log('adfasdf')
			currentTour.isStrike = true;
			currentTour.finalScore = result;
			this.nextTour();
		}
		this.finalScore();
	}
	
	secondAttempt(result: number, flag?: boolean): void {
		let currentTour: Tour = this.steps[this.currentTour]

		currentTour.secondAttempt = result;

		if(currentTour.firstAttempt + currentTour.secondAttempt === 10) {
			currentTour.isSpare = true;
		}
		currentTour.finalScore = currentTour.firstAttempt + currentTour.secondAttempt;
		//if strike in prev tour
		if(this.currentTour > 0 && this.steps[this.currentTour - 1].isStrike) {
			this.steps[this.currentTour - 1].finalScore = this.steps[this.currentTour - 1].finalScore + result;
		}
		
		this.finalScore();
	}

	firstLastAttempt(result: number, flag?: boolean): void {
		let currentTour: Tour = this.steps[this.currentTour];
		
		currentTour.firstAttempt = result;
		this.attempt = 2;
		
		//if spare in prev tour
		if(this.currentTour > 0 && this.steps[this.currentTour - 1].isSpare) {
			this.steps[this.currentTour - 1].finalScore = this.steps[this.currentTour - 1].finalScore + result;
		}
		
		//if strike in prev tour
		if(this.currentTour > 0 && this.steps[this.currentTour - 1].isStrike) {
			if(this.currentTour > 1 && this.steps[this.currentTour - 1].isStrike && this.steps[this.currentTour - 2].isStrike) {
				this.steps[this.currentTour - 2].finalScore += result;
			}
			this.steps[this.currentTour - 1].finalScore += result;
		}
		
		//check strike
		if(result === 10) {
			currentTour.isStrike = true;
			currentTour.finalScore = result;
		}
		this.finalScore();
	}

	secondLastAttempt(result: number, flag?: boolean): void {
		let currentTour: Tour = this.steps[this.currentTour]

		currentTour.secondAttempt = result;
		this.attempt = null;
		if(currentTour.firstAttempt + currentTour.secondAttempt === 10) {
			currentTour.isSpare = true;
			this.attempt = 3;
		}

		currentTour.finalScore = currentTour.firstAttempt + currentTour.secondAttempt;
		//if strike in prev tour
		if(currentTour.isStrike) {
			this.attempt = 3;
		}
		//if strike in prev tour
		if(this.steps[this.currentTour - 1].isStrike) {
			this.steps[this.currentTour - 1].finalScore += result;
		}
		
		this.finalScore();
	}


	thirdAttempt(result: number): void {
		let currentTour: Tour = this.steps[this.currentTour]

		currentTour.thirdAttempt = result;
		currentTour.finalScore += currentTour.thirdAttempt;
		this.finalScore();
		this.attempt = null;
	}

	nextTour(): void {
		this.currentTour++;
		this.attempt = 1;
	}

	checkAvailability(i: number): boolean {
		if(this.currentTour === 9) {
			return true;
		}

		const MAX_RESULT: number = 10;
		if(!this.attempt)
			return false;
		console.log('asdfasdf', this.firstAttempt, !this.firstAttempt)
		if(this.attempt === 1 && MAX_RESULT - (+this.steps[this.currentTour].secondAttempt) >= i )
			return true;

		if(this.attempt === 2 && MAX_RESULT - i >= (+this.steps[this.currentTour].firstAttempt) ) {
			return true;
		}
		return false;
	}

	finalScore(): number {
		this.finalResult = 0;
		this.steps.forEach(element => {
			this.finalResult += element.finalScore;
		});
		return 10;
	}

	selectItem(index: number) {
		this.currentTour = index;
	}
}
